package leboncoin.test;

import leboncoin.test.controller.FizzBuzzServerController;
import leboncoin.test.model.FizzBuzzRequestStatistics;
import leboncoin.test.repository.FizzBuzzRequestRepository;
import leboncoin.test.service.FizzBuzzServerService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = FizzBuzzServerController.class)
public class FizzBuzzServerControllerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private FizzBuzzServerService fizzBuzzServerService;

    @MockBean
    private FizzBuzzRequestRepository fizzBuzzRequestRepository;

    private Integer limit = 10;
    private Integer firstInt = 2;
    private Integer secondInt = 5;
    private String firstString = "fizz";
    private String secondString = "buzz";

    @Test
    @DisplayName("Testing /executeFizzBuzz without limit parameter should throw exception")
    public void testExecuteFizzBuzzWithoutLimit_should_throw_exception() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/executeFizzBuzz")
                .param("firstInt", firstInt.toString())
                .param("secondInt", secondInt.toString())
                .param("firstString", firstString)
                .param("secondString", secondString))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Testing /executeFizzBuzz without firstInt parameter should throw exception")
    public void testExecuteFizzBuzzWithoutFirstInt_should_throw_exception() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/executeFizzBuzz")
                .param("limit", limit.toString())
                .param("secondInt", secondInt.toString())
                .param("firstString", firstString)
                .param("secondString", secondString))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Testing /executeFizzBuzz without secondInt parameter should throw exception")
    public void testExecuteFizzBuzzWithoutSecondInt_should_throw_exception() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/executeFizzBuzz")
                .param("limit", limit.toString())
                .param("firstInt", firstInt.toString())
                .param("firstString", firstString)
                .param("secondString", secondString))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Testing /executeFizzBuzz without firstString parameter should throw exception")
    public void testExecuteFizzBuzzWithoutFirstString_should_throw_exception() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/executeFizzBuzz")
                .param("limit", limit.toString())
                .param("firstInt", firstInt.toString())
                .param("secondInt", secondInt.toString())
                .param("secondString", secondString))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Testing /executeFizzBuzz without secondString parameter should throw exception")
    public void testExecuteFizzBuzzWithoutSecondString_should_throw_exception() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/executeFizzBuzz")
                .param("limit", limit.toString())
                .param("firstInt", firstInt.toString())
                .param("secondInt", secondInt.toString())
                .param("firstString", firstString))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Testing /executeFizzBuzz with all parameters, should be ok")
    public void testExecuteFizzBuzz_should_be_ok() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/executeFizzBuzz")
                .param("limit", limit.toString())
                .param("firstInt", firstInt.toString())
                .param("secondInt", secondInt.toString())
                .param("firstString", firstString)
                .param("secondString", secondString))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Testing /showRequestStats with data should be ok")
    public void testShowRequestStats_should_be_ok() throws Exception {
        FizzBuzzRequestStatistics stats = new FizzBuzzRequestStatistics(limit.toString(), firstInt.toString(), secondInt.toString(), firstString, secondString, 2L);
        when(fizzBuzzRequestRepository.findFizzBuzzRequestCount()).thenReturn(Arrays.asList(stats));
        mvc.perform(MockMvcRequestBuilders
                .get("/showRequestStats"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Testing /showRequestStats without data should not be ok")
    public void testShowRequestStats_should_throw_error() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/showRequestStats"))
                .andExpect(status().isNotFound());
    }
}
