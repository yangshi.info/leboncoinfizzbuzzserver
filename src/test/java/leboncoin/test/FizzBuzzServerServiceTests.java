package leboncoin.test;

import leboncoin.test.exception.FizzBuzzDataException;
import leboncoin.test.service.FizzBuzzServerService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class FizzBuzzServerServiceTests {

    @Autowired
    FizzBuzzServerService fizzBuzzServerService;

	List<String> listParam = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");

    @Test
    @DisplayName("Testing fillResponseListWithLimit with a limit of 10")
    void testFillResponseListWithLimitFrom1To10() {
        List<String> actual = fizzBuzzServerService.fillResponseListWithLimit(10);
        List<String> expected = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
        assertThat(actual, is(expected));
        assertThat(actual, hasSize(10));
    }

    @Test
    @DisplayName("Testing fillResponseListWithLimit creating a list of string from 3 to 10")
    void testFillResponseListWithLimitFrom3To10() {
        List<String> actual = fizzBuzzServerService.fillResponseListWithLimit(3, 10);
        List<String> expected = Arrays.asList("3", "4", "5", "6", "7", "8", "9", "10");
        assertThat(actual, is(expected));
        assertThat(actual, hasSize(8));
    }

    @Test
    @DisplayName("Testing performFizzBuzzProcess without list parameter, should throw FizzBuzzDataException")
    void shouldThrowErrorListNull() {
        Exception exception = assertThrows(FizzBuzzDataException.class, () -> {
            fizzBuzzServerService.performFizzBuzzProcess(null, 3, 4, "fizz", "bizz");
        });

        String expectedMessage = "listToAlterate is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

	@Test
	@DisplayName("Testing performFizzBuzzProcess with an incorrect firstInt parameter, should throw FizzBuzzDataException")
	void shouldThrowErrorFirstIntNotCorrect() {
		Exception exception = assertThrows(FizzBuzzDataException.class, () -> {
			fizzBuzzServerService.performFizzBuzzProcess(listParam, 0, 4, "fizz", "bizz");
		});

		String expectedMessage = "firstIntegerToCheck doesn't have a correct value";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	@DisplayName("Testing performFizzBuzzProcess with an incorrect secondInt parameter, should throw FizzBuzzDataException")
	void shouldThrowErrorSecondIntNotCorrect() {
		Exception exception = assertThrows(FizzBuzzDataException.class, () -> {
			fizzBuzzServerService.performFizzBuzzProcess(listParam, 2, 0, "fizz", "bizz");
		});

		String expectedMessage = "secondIntegerToCheck doesn't have a correct value";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	@DisplayName("Testing performFizzBuzzProcess with a null firstString parameter, should throw FizzBuzzDataException")
	void shouldThrowErrorFirstStringNull() {
		Exception exception = assertThrows(FizzBuzzDataException.class, () -> {
			fizzBuzzServerService.performFizzBuzzProcess(listParam, 2, 3, null, "bizz");
		});

		String expectedMessage = "firstStringToReplaceWith is null or blank";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	@DisplayName("Testing performFizzBuzzProcess with a null firstString parameter, should throw FizzBuzzDataException")
	void shouldThrowErrorFirstStringBlank() {
		Exception exception = assertThrows(FizzBuzzDataException.class, () -> {
			fizzBuzzServerService.performFizzBuzzProcess(listParam, 2, 3, "", "bizz");
		});

		String expectedMessage = "firstStringToReplaceWith is null or blank";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	@DisplayName("Testing performFizzBuzzProcess with a null secondString parameter, should throw FizzBuzzDataException")
	void shouldThrowErrorSecondStringNull() {
		Exception exception = assertThrows(FizzBuzzDataException.class, () -> {
			fizzBuzzServerService.performFizzBuzzProcess(listParam, 2, 3, "fizz", null);
		});

		String expectedMessage = "secondStringToReplaceWith is null or blank";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	@DisplayName("Testing performFizzBuzzProcess with a null secondString parameter, should throw FizzBuzzDataException")
	void shouldThrowErrorSecondStringBlank() {
		Exception exception = assertThrows(FizzBuzzDataException.class, () -> {
			fizzBuzzServerService.performFizzBuzzProcess(listParam, 2, 3, "fizz", "");
		});

		String expectedMessage = "secondStringToReplaceWith is null or blank";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	@DisplayName("Testing fillResponseListWithLimit with a limit of 10")
	void testPerformFizzBuzzProcess_shouldBeOk() {
		listParam = fizzBuzzServerService.performFizzBuzzProcess(listParam, 2, 5, "fizz", "buzz");
		List<String> expected = Arrays.asList("1", "fizz", "3", "fizz", "buzz", "fizz", "7", "fizz", "9", "fizzbuzz");
		assertThat(listParam, is(expected));
		assertThat(listParam, hasSize(10));
    }
}
