package leboncoin.test.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "fizz_buzz_request")
@Data
@NoArgsConstructor
public class FizzBuzzRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "limit_range")
    private String limit;

    @Column(name = "first_int")
    private String firstInt;

    @Column(name = "second_int")
    private String secondInt;

    @Column(name = "first_string")
    private String firstString;

    @Column(name = "second_string")
    private String secondString;

    public FizzBuzzRequest(String limit, String firstInt, String secondInt, String firstString, String secondString) {
        this.limit = limit;
        this.firstInt = firstInt;
        this.secondInt = secondInt;
        this.firstString = firstString;
        this.secondString = secondString;
    }
}
