package leboncoin.test.model;


import lombok.Data;

@Data
public class FizzBuzzRequestStatistics {
    private final String limit;
    private final String firstInt;
    private final String secondInt;
    private final String firstString;
    private final String secondString;
    private final Long count;
}
