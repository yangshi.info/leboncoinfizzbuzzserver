package leboncoin.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FizzbuzzrestserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(FizzbuzzrestserverApplication.class, args);
	}

}
