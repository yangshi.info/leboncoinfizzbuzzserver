package leboncoin.test.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FizzBuzzDataException extends RuntimeException {

    /**
     * Message constructor.
     *
     * @param msg The message
     */
    public FizzBuzzDataException(String msg) {
        super(msg);
    }

    /**
     * Message constructor.
     *
     * @param cause the cause
     */
    public FizzBuzzDataException(Throwable cause) {
        super(cause);
    }

    public FizzBuzzDataException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
