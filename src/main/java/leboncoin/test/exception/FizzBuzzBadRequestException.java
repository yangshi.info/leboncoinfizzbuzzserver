package leboncoin.test.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FizzBuzzBadRequestException extends RuntimeException {

    /**
     * Message constructor.
     *
     * @param msg The message
     */
    public FizzBuzzBadRequestException(String msg) {
        super(msg);
    }

    /**
     * Message constructor.
     *
     * @param cause the cause
     */
    public FizzBuzzBadRequestException(Throwable cause) {
        super(cause);
    }

    public FizzBuzzBadRequestException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
