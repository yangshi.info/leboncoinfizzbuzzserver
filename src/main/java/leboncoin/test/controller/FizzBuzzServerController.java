package leboncoin.test.controller;

import io.micrometer.core.instrument.util.StringUtils;
import leboncoin.test.exception.FizzBuzzBadRequestException;
import leboncoin.test.exception.FizzBuzzDataException;
import leboncoin.test.model.FizzBuzzRequest;
import leboncoin.test.model.FizzBuzzRequestStatistics;
import leboncoin.test.repository.FizzBuzzRequestRepository;
import leboncoin.test.service.FizzBuzzServerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FizzBuzzServerController {

    @Autowired
    FizzBuzzServerService fizzBuzzServerService;

    @Autowired
    FizzBuzzRequestRepository fizzBuzzRequestRepository;

    private static final Logger log = LoggerFactory.getLogger(FizzBuzzServerController.class);

    @GetMapping("/executeFizzBuzz")
    @ResponseBody
    public List<String> executeFizzBuzz(@RequestParam(name = "limit", required = false) Integer limit,
                                        @RequestParam(name = "firstInt", required = false) Integer firstIntToReplace,
                                        @RequestParam(name = "secondInt", required = false) Integer secondIntToReplace,
                                        @RequestParam(name = "firstString", required = false) String firstStringToReplaceWith,
                                        @RequestParam(name = "secondString", required = false) String secondStringToReplaceWith) {
        log.info("/executeFizzBuzz with these parameters : limit={}, firstInt={}, secondInt={}, firstString={}, secondString={}",
                limit, firstIntToReplace, secondIntToReplace, firstStringToReplaceWith, secondStringToReplaceWith);

        if (limit == null || limit.intValue() < 0) {
            throw new FizzBuzzBadRequestException("limit parameter isn't correct : limit=" + limit);
        }

        if (firstIntToReplace == null || firstIntToReplace.intValue() < 1) {
            throw new FizzBuzzBadRequestException("firstInt parameter isn't correct : firstInt=" + firstIntToReplace);
        }

        if (secondIntToReplace == null || secondIntToReplace.intValue() < 1) {
            throw new FizzBuzzBadRequestException("secondInt parameter isn't correct : secondInt=" + secondIntToReplace);
        }

        if (StringUtils.isBlank(firstStringToReplaceWith)) {
            throw new FizzBuzzBadRequestException("firstString parameter isn't correct : firstString=" + firstStringToReplaceWith);
        }

        if (StringUtils.isBlank(secondStringToReplaceWith)) {
            throw new FizzBuzzBadRequestException("secondString parameter isn't correct : secondString=" + secondStringToReplaceWith);
        }

        FizzBuzzRequest requestToSave = new FizzBuzzRequest(limit.toString(), firstIntToReplace.toString(), secondIntToReplace.toString(), firstStringToReplaceWith, secondStringToReplaceWith);
        fizzBuzzRequestRepository.save(requestToSave);
        log.info("request={} saved", requestToSave.toString());

        List<String> listStringToReturn = fizzBuzzServerService.fillResponseListWithLimit(limit.intValue());
        log.info("The list content after filling with number range={}", listStringToReturn.toString());
        try {
            listStringToReturn = fizzBuzzServerService.performFizzBuzzProcess(listStringToReturn, firstIntToReplace, secondIntToReplace, firstStringToReplaceWith, secondStringToReplaceWith);
        } catch (FizzBuzzDataException e) {
            log.error(e.getMessage());
        }
        log.info("The list content after processing the fizzbuzz={}", listStringToReturn.toString());
        return listStringToReturn;
    }

    @GetMapping("/showRequestStats")
    public Map<FizzBuzzRequest, Long> showRequestStats() {
        log.info("/showRequestStats find the most common request...");
        List<FizzBuzzRequestStatistics> foundStatistics = fizzBuzzRequestRepository.findFizzBuzzRequestCount();
        if (foundStatistics == null || foundStatistics.isEmpty()) {
            throw new FizzBuzzDataException("didn't find any statistics yet...");
        }
        Map<FizzBuzzRequest, Long> resultToReturn = new HashMap<>();
        resultToReturn.put(new FizzBuzzRequest(foundStatistics.get(0).getLimit(),
                        foundStatistics.get(0).getFirstInt(),
                        foundStatistics.get(0).getSecondInt(),
                        foundStatistics.get(0).getFirstString(),
                        foundStatistics.get(0).getSecondString()),
                foundStatistics.get(0).getCount());

        return resultToReturn;
    }
}
