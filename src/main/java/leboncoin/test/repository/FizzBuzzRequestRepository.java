package leboncoin.test.repository;

import leboncoin.test.model.FizzBuzzRequest;
import leboncoin.test.model.FizzBuzzRequestStatistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FizzBuzzRequestRepository extends JpaRepository<FizzBuzzRequest, Long> {

    @Query("SELECT new leboncoin.test.model.FizzBuzzRequestStatistics(request.limit, request.firstInt, request.secondInt, request.firstString, request.secondString, COUNT(request)) "
            + " FROM FizzBuzzRequest request "
            + " GROUP BY request.limit, request.firstInt, request.secondInt, request.firstString, request.secondString  "
            + " ORDER BY COUNT(request) DESC")
    List<FizzBuzzRequestStatistics> findFizzBuzzRequestCount();
}
