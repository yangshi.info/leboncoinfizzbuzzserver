package leboncoin.test.service;

import io.micrometer.core.instrument.util.StringUtils;
import leboncoin.test.exception.FizzBuzzDataException;
import leboncoin.test.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Yang SHI the 18/10/2021
 * Service for FizzBuzzServer
 */

@Service
public class FizzBuzzServerService {

    private static final Logger log = LoggerFactory.getLogger(FizzBuzzServerService.class);

    /**
     * Fill a list string including numbers of a specified range
     *
     * @param start the start of the number range
     * @param limit the limit of the number range
     * @return generated list string including numbers from the start to the limit
     */
    public List<String> fillResponseListWithLimit(int start, int limit) {
        return IntStream.rangeClosed(start, limit).mapToObj(String::valueOf).collect(Collectors.toList());
    }

    /**
     * Fill a list string including numbers of a specified range
     * Using the default start number
     *
     * @param limit the limit of the number range
     * @return generated list string including numbers from the start to the limit
     */
    public List<String> fillResponseListWithLimit(int limit) {
        return fillResponseListWithLimit(Constants.FIZZBUZZ_DEFAULT_START, limit);
    }

    /**
     * Processing to the Fizz Buzz algorythm element replacement
     *
     * @param listToCheck               the list containing the elements to check
     * @param firstIntegerToCheck       the first integer to divide
     * @param secondIntegerToCheck      the second integer to divide
     * @param firstStringToReplaceWith  the first string to replace with the number which can be divided by firstIntegerToCheck
     * @param secondStringToReplaceWith the second string to replace with the number which can be divided by secondIntegerToCheck
     * @return altered list from the Fizz Buzz replacement
     * @throws FizzBuzzDataException
     */
    public List<String> performFizzBuzzProcess(List<String> listToCheck, int firstIntegerToCheck,
                                               int secondIntegerToCheck, String firstStringToReplaceWith,
                                               String secondStringToReplaceWith) throws FizzBuzzDataException {
        if (listToCheck == null) {
            throw new FizzBuzzDataException("listToAlterate is null");
        }

        if (firstIntegerToCheck < 1) {
            throw new FizzBuzzDataException("firstIntegerToCheck doesn't have a correct value");
        }

        if (secondIntegerToCheck < 1) {
            throw new FizzBuzzDataException("secondIntegerToCheck doesn't have a correct value");
        }

        if (StringUtils.isBlank(firstStringToReplaceWith)) {
            throw new FizzBuzzDataException("firstStringToReplaceWith is null or blank");
        }

        if (StringUtils.isBlank(secondStringToReplaceWith)) {
            throw new FizzBuzzDataException("secondStringToReplaceWith is null or blank");
        }

        String combinedStringToReplace = firstStringToReplaceWith + secondStringToReplaceWith;

        List<String> alteredList = new ArrayList<>();
        listToCheck.stream().forEach(stringToAlternate -> {
            int stringToCheckIntValue = Integer.valueOf(stringToAlternate);
            boolean isCheckingElementDivisibleByTheFirstInteger = stringToCheckIntValue % firstIntegerToCheck == 0 ? true : false;
            boolean isCheckingElementDivisibleByTheSecondInteger = stringToCheckIntValue % secondIntegerToCheck == 0 ? true : false;
            if (isCheckingElementDivisibleByTheFirstInteger && isCheckingElementDivisibleByTheSecondInteger) {
                stringToAlternate = combinedStringToReplace;
            } else if (isCheckingElementDivisibleByTheFirstInteger) {
                stringToAlternate = firstStringToReplaceWith;
            } else if (isCheckingElementDivisibleByTheSecondInteger) {
                stringToAlternate = secondStringToReplaceWith;
            }

            alteredList.add(stringToAlternate);
        });

        return alteredList;
    }
}
