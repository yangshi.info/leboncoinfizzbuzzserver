package leboncoin.test.rest;

import leboncoin.test.exception.FizzBuzzBadRequestException;
import leboncoin.test.exception.FizzBuzzDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(FizzBuzzBadRequestException.class)
    public ResponseEntity<Map<String, String>> handleFizzBuzzBadRequestException(FizzBuzzBadRequestException e, HttpServletResponse response) throws IOException {
        log.error("handleFizzBuzzBadRequestException", e);
        Map<String, String> errorResponse = new HashMap<>();
        errorResponse.put("message", e.getLocalizedMessage());
        errorResponse.put("status", HttpStatus.BAD_REQUEST.toString());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<Map<String, String>> handleMethodArgumentTypeMismatch(final MethodArgumentTypeMismatchException e, HttpServletResponse response) throws IOException {
        Map<String, String> errorResponse = new HashMap<>();
        log.error(e.getClass().getName(), e);
        final String error = e.getName() + " should be of type " + e.getRequiredType().getName();
        errorResponse.put("message", error);
        errorResponse.put("status", HttpStatus.BAD_REQUEST.toString());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        String msg = "Unable to process incoming request [" + request.getDescription(false) + "]: " + ex.getMessage();
        return handleExceptionInternal(ex, msg, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
                                                             HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (body != null) {
            log.error("{}", body);
        } else {
            log.error("Handling {}: {}", ex.getClass().getCanonicalName(), ex.getMessage());
        }
        return super.handleExceptionInternal(ex, body, headers, status, request);
    }

    @ExceptionHandler(FizzBuzzDataException.class)
    public ResponseEntity<Map<String, String>> handleFizzBuzzDataException(FizzBuzzDataException e, HttpServletResponse response) throws IOException {
        log.error("handleFizzBuzzBadRequestException", e);
        Map<String, String> errorResponse = new HashMap<>();
        errorResponse.put("message", e.getLocalizedMessage());
        errorResponse.put("status", HttpStatus.NOT_FOUND.toString());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}
